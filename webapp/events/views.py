# webapp/events/views.py
from flask import render_template, Blueprint, flash
from .forms import NewEventForm
from webapp.models import db, Webevent

events_blueprint = Blueprint(
    'events',
    __name__
)


@events_blueprint.route('/', methods=['GET','POST'])
def events_main():
    form = NewEventForm()
    if form.validate_on_submit():
        sname = form.sname.data
        descr = form.description.data
        planned = form.planned.data
        existing_event = Webevent.query.filter_by(sname=sname).first()
        if existing_event is None:
            new_event = Webevent(
                sname=sname,
                description=descr,
                planned=planned)
            db.session.add(new_event)
            db.session.commit()
            flash("{} was registered as event".format(form.sname.data))
        else:
            flash("{} already registered as an event".format(form.sname.data))
        return render_template('events.html')
    return render_template('events.html', form=form)
