# webapp/__init__.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
# from flask_redis import FlaskRedis

db = SQLAlchemy()
# r = FlaskRedis()


# Globally accessible libraries
from webapp.home.views import home_blueprint
from webapp.events.views import events_blueprint
from webapp.newsletter.views import newsletter_blueprint



def create_app():
    """Initialize the core application."""
    app = Flask(__name__, instance_relative_config=False)
    # app.config.from_object('config.Config')
    # Using a production configuration
    # app.config.from_object('config.ProdConfig')

    # Using a development configuration
    app.config.from_object('config.DevConfig')

    # Initialize Plugins
    db.init_app(app)
    migrate = Migrate(app, db)

    # r.init_app(app)

    with app.app_context():
        # Include our Routes
        from webapp.home import views
        from webapp.newsletter import views
        from webapp.events import views

        # # Register Blueprints
        app.register_blueprint(home_blueprint)
        app.register_blueprint(events_blueprint, url_prefix='/events')
        app.register_blueprint(newsletter_blueprint, url_prefix='/newsletter')

        # create database and tables
        db.create_all()

        return app
